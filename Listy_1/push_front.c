//
// Created by Monika on 2020-05-18.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "moduly.h"

// Add element to the beginning of a list (new head)
void push_front(struct Client **head, char *p_surname)
{
    struct Client *new;
    new = (struct Client*)malloc(sizeof(struct Client));

    strcpy(new->surname, p_surname);

    new->next=*head;

    *head = new; // new element is a head now

    printf("Dodano pomyslnie");

    return;
}
