//
// Created by Monika on 2020-05-18.
//

#ifndef LISTY_1_MODULY_H
#define LISTY_1_MODULY_H


struct Client
        {
            char surname[50];
            struct Client *next;
        };
int list_size(struct Client *head);
void show_list(struct Client *head);
void push_front(struct Client **head, char *p_surname);
void pop_by_surname(struct Client **head);


#endif //LISTY_1_MODULY_H
