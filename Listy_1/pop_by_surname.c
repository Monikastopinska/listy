//
// Created by Monika on 2020-05-18.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "moduly.h"

// Delete first element of given name
void pop_by_surname(struct Client **head){
    char DeleteSurname[50];
    struct Client *temp, *previous;
    temp = *head;

    printf("Podaj nazwisko które chcesz usunąc?\n");
    scanf("%s", DeleteSurname);

    //zaalokowanie
    while(temp->next != NULL)
    {
        if(strcmp(DeleteSurname, temp->surname)==0)
        {
            break;
        }
        previous= temp;
        temp = temp->next;
    }

    // usuwanie.
    if ( temp != NULL )
    {
        if ( temp == *head )
        {
            *head = temp->next;
        }
        else
        {
            previous->next = temp->next;
        }

        free(temp);
        printf("Usunieto pomyslnie\n");
    }
    return;
}


